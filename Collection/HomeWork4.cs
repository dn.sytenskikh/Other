﻿using System;
using System.Collections.Generic;

namespace Collection
{
    public class HomeWork4
    {
        public static void Main()
        {
            // Задание 2
            List<int> list1 = new List<int>() { 1, 2, 2, 4, 2, 1, 4, 4, 2, 2, 4, 2, 1, 3, 4, 5, 8, 3, 5, 6, 3, 4 };
            IntegerCounter(list1);


            GeneralizedCounter el1 = new GeneralizedCounter(1, "string1");
            GeneralizedCounter el2 = new GeneralizedCounter(2, "string2");
            GeneralizedCounter el3 = new GeneralizedCounter(3, "string3");
            List<GeneralizedCounter> list2 = new List<GeneralizedCounter>() { el1, el1, el2, el2, el2, el1, el3, el2 };
            MyClassCounter(list2);

            List<string> list3 = new List<string>() { "1", "2", "2", "4", "2", "1", "4", "4", "2", "2", "4", "2", "1", "3", "4", "5", "8", "3", "5", "6", "3", "4" };
            LinqCounter(list3);


            //Задание 3

            //Я не пойму почему у меня не рабоет прям пример из методички (не копипаст, а ввод вручную, копипаст от туда никогда не работал)
            //Может я туплю и чего то не замечаю, но метод OrderBy не находится, я сделал ниже предположение как возможно хотелось чтоб я решил задау
            //Но код я запустить не смог

            Dictionary<string, int> dict = new Dictionary<string, int>(){
                {"four", 4},
                {"two", 2},
                {"one", 1},
                {"three", 3},
            };

//            var d = dict.OrderBy(delegate (KeyValuePair<string, int> pair) { return pair.Value; });
//            foreach (var pair in d){
//                Console.WriteLine($"{pair.Key} - {pair.Value}");
//            }
//
//            var d = dict.OrderBy((KeyValuePair<string, int> pair) => pair.Value);
//            foreach (var pair in d)
//            {
//                Console.WriteLine($"{pair.Key} - {pair.Value}");
//            } 
        }

        public static void IntegerCounter(List<int> list)
        {
            Dictionary<int, int> dic = new Dictionary<int, int>();

            foreach (int el in list)
            {
                dic.TryGetValue(el, out int val);
                dic[el] = ++val;
            }

            foreach (int el in dic.Keys)
            {
                Console.WriteLine($"Число {el} встречалось в списке {dic[el]} раз");
            }
        }

        public static void MyClassCounter<T>(List<T> source)
        {
            Dictionary<T, int> dic = new Dictionary<T, int>();

            foreach (T el in source)
            {
                dic.TryGetValue(el, out int val);
                dic[el] = ++val;
            }

            foreach (T el in dic.Keys)
            {
                Console.WriteLine($"Элемент {el} встречался в списке {dic[el]} раз");

            }
        }

        public static void LinqCounter(List<string> list)
        {
            //Не знаю как сделать, потом разберусь, в Шилдте целая глава по LINQ     
        }
    }

    public class GeneralizedCounter
    {
        public int intField;
        public string strField;

        public GeneralizedCounter(int field1, string field2)
        {
            intField = field1;
            strField = field2;
     
        }

        public override string ToString()
        {
            return $"int field: {intField}, string field: {strField}";
        }
    }
}
